#ifndef BOARD_H_
#define BOARD_H_

#include "Enums.h"
#include "Struct.h"
#include "WinningMoveChecker.h"

//Represents a board used in a game of Connect 4
//Responsible for storing and retrieving the contents of the cells within the board
class CBoard
{
public:
	//Construtor, allows for variable board size
	CBoard(SBoardSize sBoardSize, int iWinningLineLength);
	~CBoard();

	//Resets every cell's state to empty
	void ClearBoard();

	//Returns the size of the board as a BoardSize struct
	SBoardSize GetBoardSize() const;

	//Returns the area of the board
	int GetBoardArea() const;

	//Returns the size of line that a player must make with their tokens in order to win the game
	int GetWinningLineLength() const;

	//Returns the state of the cell the provided coordinates
	//Assumes coordinates are valid for this board size
	ECellState GetCellState(SCellCoordinates sCoordinates) const;

	//Returns true if there are no empty cells on the board, otherwise returns false
	bool IsBoardFull() const;

	//Returns true if there are no empty cells in the column (indexed by the provided value), otherwise returns false
	bool IsColumnFull(int iColumn) const;

	//Inserts a token for the privded player into the provided column, then calculates if this insertion formed a winning line
	//Required to pass in which form of move the insertion was (special or normal)
	//Assumes that the column provided is not full
	void FillColumn(int iColumn, EPlayer ePlayer, EMoveType eMoveType);

	//Returns the player who was first to achieve a winning line, if no winning line present, returns EPlayer_None
	EPlayer GetWinningPlayer() const;

	//Returns true if the provided column index is within the bounds of the board, otherwise returns false
	bool IsColumnIndexInBounds(int iColumn) const;

	//Returns true if the provided row index is within the bounds of the board, otherwise returns false
	bool IsRowIndexInBounds(int iRow) const;

	//Returns true if the provided cell coordinates is within the bounds of the board, otherwise returns false
	bool AreCoordinatesInBounds(SCellCoordinates sCoordinates) const;

	//Returns the coordinates of the next cell from the starting point in the given direction
	SCellCoordinates GetNextCellCoordinatesInDirection(SCellCoordinates sStartingCoordinates, EDirection eDirection) const;

private:
	//The contents of the cells are stored as an array of enums, index by values gained from the GetCellIndex method
	ECellState* m_paeBoard;

	//Object that checks to see if a particular move created a winning line on the board
	CWinningMoveChecker m_cWinningMoveChecker;

	//The size of the board
	SBoardSize m_sBoardSize;

	//The length of a winning line
	int m_iWinningLineLength;

	//If a winning move is present, the player who owns that line, else EPlayer_None
	EPlayer m_eWinningPlayer;

	//Returns the index the pointer array relating to the celll at the given coordinates
	int GetCellIndex(SCellCoordinates sCoordinates) const;

	//Returns the CellState for cells containing a token owned by the given player
	ECellState GetCellStateForPlayer(EPlayer ePlayer) const;

	//Returns the coordinates for the highest empty cell in the given column
	//Assumed column is not full
	SCellCoordinates GetHighestEmptyCellInColumn(int iColumn) const;

	//Sets the state of the cell at the given coordinates to the given CellState
	void SetCellState(SCellCoordinates sCoordinates, ECellState eCellState);

	//Applies the move of the given type at the given coordinates, will trigger the effects on any special moves
	void ExecuteMove(SCellCoordinates sMoveCoordinates, ECellState eCurrentPlayerCellState, EMoveType eMoveType);

	//Reverses the order of the tokens inserted into a given row, special move available to players
	void MirrorRow(int iRow);

	//Removes all token from a given row, special move available to players
	void ClearRow(int iRow);

	//Removes all tokens with a 1 cell radius (vertically, horizontally, diagonally) from the given coordinates, special move available to players
	void Bomb(SCellCoordinates sCoordinates);

	//Ensures that all tokens are in the lowest row possible, called after using special moves that may leave tokens floating in midair
	void CollapseRows();

	
};

#endif