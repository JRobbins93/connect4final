#include "stdafx.h"
#include "Game.h"

using namespace std;

int main()
{
	//Alter the integer values here to change the configuration of the game
	SBoardSize sBoardSize;
	sBoardSize.iBoardWidth = 7;
	sBoardSize.iBoardHeight = 6;

	SGameConfiguration sGameConfiguration;
	sGameConfiguration.iWinningLineLength = 4;
	sGameConfiguration.sBoardSize = sBoardSize;

	CGame cGame(sGameConfiguration);
	cGame.RunGameLoop();

	return 0;
}