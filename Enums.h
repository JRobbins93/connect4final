#ifndef ENUMS_H_
#define ENUMS_H_

//Enums that are used by multiple classes within the program

//The various states a game of Connect 4 can be in
enum EGameState
{
	EGameState_Playing,
	EGameState_P1Won,
	EGameState_P2Won,
	EGameState_Draw
};

//The various states that a cell can be in
enum ECellState
{
	ECellState_Empty,
	ECellState_P1,
	ECellState_P2
};

//The different types of input that a player can give
enum EInputType
{
	EInputType_None,
	EInputType_ValidMove,
	EInputType_FullColumn,
	EInputType_FailedSpecial,
	EInputType_Invalid
};

//Values representing the two players of a game of Connect 4, or no player at all
enum EPlayer
{
	EPlayer_None,
	EPlayer_P1,
	EPlayer_P2
};

//The different types of move that a player can perform, including specials
enum EMoveType
{
	EMoveType_Normal,
	EMoveType_ClearRow,
	EMoveType_MirrorRow,
	EMoveType_Bomb,
	EMoveType_Invalid
};

//Directions used when searching for a winning line
enum EDirection
{
	EDirection_Up,
	EDirection_UpRight,
	EDirection_Right,
	EDirection_DownRight,
	EDirection_Down,
	EDirection_DownLeft,
	EDirection_Left,
	EDirection_UpLeft
};

#endif