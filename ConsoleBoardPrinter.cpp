#include "stdafx.h"
#include "ConsoleBoardPrinter.h"
#include <iostream>

using namespace std;

CConsoleBoardPrinter::CConsoleBoardPrinter(CBoard& cBoard)
	: m_cBoard(cBoard)
{
	m_hwndConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	SBoardSize sBoardSize = cBoard.GetBoardSize();
	m_iBoardWidth = sBoardSize.iBoardWidth;
	m_iBoardHeight = sBoardSize.iBoardHeight;
}


CConsoleBoardPrinter::~CConsoleBoardPrinter()
{
}

void CConsoleBoardPrinter::PrintBoard()
{
	ClearConsole();
	PrintColumnNumbersRow();
	PrintBoardContents();
	PrintHorizontalSeparatorRow();
	PrintColumnNumbersRow();
	cout << endl;
}

int CConsoleBoardPrinter::CellStateToColourCode(ECellState eCellState)
{
	int result;

	switch (eCellState)
	{
	case ECellState_P1:
		result = k_iColourCodeRed;
		break;
	case ECellState_P2:
		result = k_iColourCodeYellow;
		break;
	default:
		result = k_iColourCodeWhite;
		break;
	}

	return result;
}

char CConsoleBoardPrinter::CellStateToRepresentation(ECellState eCellState)
{
	char result;

	switch (eCellState)
	{
	case ECellState_Empty:
		result = k_chEmptyCellRepresentation;
		break;
	case ECellState_P1:
	case ECellState_P2:
		result = k_chFilledCellRepresentation;
		break;
	default:
		result = k_chEmptyCellRepresentation;
	}

	return result;
}

void CConsoleBoardPrinter::ClearConsole()
{
	system("cls");
}

void CConsoleBoardPrinter::PrintColumnNumbersRow()
{
	SetConsoleColour(k_iColourCodeWhite);

	for (int iColumn = 0; iColumn < m_iBoardWidth; ++iColumn)
	{
		cout << "  " << (iColumn + 1) << " "; //Column numbers printed to screen are indexed from 1, not 0
	}

	cout << endl;
}

void CConsoleBoardPrinter::PrintBoardContents()
{
	for (int iRow = m_iBoardHeight - 1; iRow >= 0; --iRow) //Print highest row first, else board will be drawn upside down
	{
		PrintHorizontalSeparatorRow();
		PrintCellRow(iRow);
	}
}

void CConsoleBoardPrinter::PrintHorizontalSeparatorRow()
{
	SetConsoleColour(k_iColourCodeBlue);

	cout << k_chSeparatorJoin;

	for (int iIndex = 0; iIndex < m_iBoardWidth; ++iIndex)
	{
		cout << k_chSeparatorHorizontal << k_chSeparatorHorizontal << k_chSeparatorHorizontal << k_chSeparatorJoin;
	}
	
	cout << endl;
}

void CConsoleBoardPrinter::PrintCellRow(int iRow)
{
	cout << k_chSeparatorVertical;

	for (int iColumn = 0; iColumn < m_iBoardWidth; ++iColumn)
	{
		SCellCoordinates sCurrentCellCoordinates;
		sCurrentCellCoordinates.iColumn = iColumn;
		sCurrentCellCoordinates.iRow = iRow;

		ECellState eCurrentCellState = m_cBoard.GetCellState(sCurrentCellCoordinates);

		char chCurrentCellStateRepresentation = CellStateToRepresentation(eCurrentCellState);

		int iCurrentCellStateColourCode = CellStateToColourCode(eCurrentCellState);
		SetConsoleColour(iCurrentCellStateColourCode);

		//Added spaces around the contents of cell as the templated version would confuse players when they were looking at diagonals
		cout << ' ' << chCurrentCellStateRepresentation; 

		SetConsoleColour(k_iColourCodeBlue);
		cout << ' ' << k_chSeparatorVertical;
	}

	cout << endl;
}

void CConsoleBoardPrinter::SetConsoleColour(int iConsoleColourCode)
{
	m_eCurrentConsoleColourCode = iConsoleColourCode;
	SetConsoleTextAttribute(m_hwndConsole, iConsoleColourCode);
}
