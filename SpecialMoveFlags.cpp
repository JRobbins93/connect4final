#include "stdafx.h"
#include "SpecialMoveFlags.h"


CSpecialMoveFlags::CSpecialMoveFlags()
{
	Reset(); //Initialise all specials to be unused
}

CSpecialMoveFlags::~CSpecialMoveFlags()
{
}

bool CSpecialMoveFlags::GetMirrorRowUsed() const
{
	return m_bMirrorRowUsed;
}

void CSpecialMoveFlags::SetMirrorRowUsed(bool value)
{
	m_bMirrorRowUsed = value;
}

bool CSpecialMoveFlags::GetClearRowUsed() const
{
	return m_bClearRowUsed;
}

void CSpecialMoveFlags::SetClearRowUsed(bool value)
{
	m_bClearRowUsed = value;
}

bool CSpecialMoveFlags::GetBombUsed() const
{
	return m_bBombUsed;
}

void CSpecialMoveFlags::SetBombUsed(bool value)
{
	m_bBombUsed = value;
}

void CSpecialMoveFlags::Reset()
{
	m_bMirrorRowUsed = false;
	m_bClearRowUsed = false;
	m_bBombUsed = false;
}
