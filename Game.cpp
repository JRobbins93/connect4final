#include "stdafx.h"
#include "Game.h"
#include "stdlib.h"
#include "time.h"
#include <iostream>
#include <string>

using namespace std;

CGame::CGame(SGameConfiguration sGameConfiguration)
	: m_bQuitApplication(false)
	, m_bQuitGame(false)
	, m_bGameOver(false)
	, m_cBoard(sGameConfiguration.sBoardSize, sGameConfiguration.iWinningLineLength)
	, m_cConsoleBoardPrinter(m_cBoard)
{	
	SeedRandomNumberGeneration();
}

CGame::~CGame()
{
}

void CGame::RunGameLoop()
{
	while (!m_bQuitApplication)
	{
		Reset();

		while (!IsGameOver())
		{
			if (m_ePreviousInputType == EInputType_ValidMove)
			{
				SwitchCurrentPlayer();
			}

			m_cConsoleBoardPrinter.PrintBoard();

			PrintMessages();

			HandlePlayerMove();
		}

		if (m_bGameOver) //If the game was played to the end, and not quit prematurely, declare the result
		{
			m_cConsoleBoardPrinter.PrintBoard();
			m_cMessagePrinter.PrintGameResultMessage(m_eCurrentGameState);
		}

		m_bQuitApplication = !HandleRequestAnotherGame(); //If user doesn't want another game, application will close
	}
}

bool CGame::IsGameOver()
{
	return m_bQuitGame || m_bGameOver;
}

void CGame::Reset()
{
	RandomisePlayer();

	m_eCurrentGameState = EGameState_Playing;
	m_ePreviousInputType = EInputType_None;

	m_cBoard.ClearBoard();

	ResetSpecialMoveFlags();

	m_bQuitGame = false;
	m_bGameOver = false;
}

void CGame::SeedRandomNumberGeneration()
{
	srand((unsigned)time(NULL));
}

void CGame::RandomisePlayer()
{
	if ((rand() & 1) == 0)
	{
		m_eCurrentPlayer = EPlayer_P1;
	}
	else
	{
		m_eCurrentPlayer = EPlayer_P2;
	}
}

void CGame::ResetSpecialMoveFlags()
{
	m_cP1SpecialMoveFlags.Reset();
	m_cP2SpecialMoveFlags.Reset();
}

void CGame::SwitchCurrentPlayer()
{
	if (m_eCurrentPlayer == EPlayer_P1)
	{
		m_eCurrentPlayer = EPlayer_P2;
	}
	else
	{
		m_eCurrentPlayer = EPlayer_P1;
	}
}

void CGame::PrintMessages()
{
	m_cMessagePrinter.PrintCurrentPlayerMessage(m_eCurrentPlayer);
	m_cMessagePrinter.PrintPreviousInputFeedbackMessage(m_ePreviousInputType);

	CSpecialMoveFlags& cCurrentPlayerSpecialMoveFlags = GetSpecialMoveFlagsForPlayer(m_eCurrentPlayer);
	m_cMessagePrinter.PrintCurrentPlayerRemainingSpecialMovesMessage(cCurrentPlayerSpecialMoveFlags);
	m_cMessagePrinter.PrintInstructionMessage();
}

void CGame::HandlePlayerMove()
{
	char chInput1;
	char chInput2;
	
	ReadCharFromStdIn(chInput1, chInput2);

	m_bQuitGame = CheckForQuitGame(chInput1);

	if (m_bQuitGame) //Player has quit, can exit method
	{
		return;
	}

	int iInputColumn = CharToInt(chInput1);
	EMoveType eInputMoveType = CharToMoveType(chInput2);

	bool bMoveIsValid = ValidateMove(iInputColumn, eInputMoveType);

	if (bMoveIsValid)
	{
		ExecuteMove(iInputColumn, eInputMoveType);

		CheckForGameOver();
	}
}

bool CGame::ValidateMove(int iInputColumn, EMoveType eMoveType)
{
	//Will also return false if the player puts in random, non-numerical chars
	bool bIsColumnInBounds = m_cBoard.IsColumnIndexInBounds(iInputColumn);

	if (!bIsColumnInBounds || eMoveType == EMoveType_Invalid)
	{
		m_ePreviousInputType = EInputType_Invalid;
		return false;
	}

	bool bIsColumnFull = m_cBoard.IsColumnFull(iInputColumn);

	if (bIsColumnFull)
	{
		m_ePreviousInputType = EInputType_FullColumn;
		return false;
	}

	bool bIsSpecialAlreadyUsed = CheckForPreviouslyUsedSpecialMove(eMoveType);

	if (bIsSpecialAlreadyUsed)
	{
		m_ePreviousInputType = EInputType_FailedSpecial;
		return false;
	}

	return true; //Move is valid if it has passed all previous checks
}

void CGame::ReadCharFromStdIn(char& chInput)
{
	cin.clear();
	chInput = cin.get();

	cin.ignore(1000, '\n');
}

void CGame::ReadCharFromStdIn(char& chInput1, char& chInput2)
{
	cin.clear();

	chInput1 = cin.get();
	chInput2 = cin.get();

	if (chInput2 != '\n')
	{
		cin.ignore(1000, '\n');
	}
}

int CGame::CharToInt(char chInput)
{
	return chInput - '1';
}

EMoveType CGame::CharToMoveType(char chInput)
{
	switch (chInput)
	{
	case 'm':
		return EMoveType_MirrorRow;
	case 'c':
		return EMoveType_ClearRow;
	case 'b':
		return EMoveType_Bomb;
	case '\n':
		return EMoveType_Normal;
	default:
		return EMoveType_Invalid;
	}
}

bool CGame::CheckForQuitGame(char chInput)
{
	bool bAttemptingToQuit = (chInput == 'Q' || chInput == 'q');

	bool bConfirmQuit = false;

	if (bAttemptingToQuit)
	{
		bConfirmQuit = ConfirmQuitGame();
	}

	return bConfirmQuit;
}

bool CGame::ConfirmQuitGame()
{
	m_cMessagePrinter.PrintQuitConfirmationMessage();

	char chInput;
	ReadCharFromStdIn(chInput);

	return (chInput == 'y' || chInput == 'Y');
}

bool CGame::CheckForPreviouslyUsedSpecialMove(EMoveType eSelectedMoveType)
{
	CSpecialMoveFlags& cCurrentPlayerSpecialMoveFlags = GetSpecialMoveFlagsForPlayer(m_eCurrentPlayer);

	switch (eSelectedMoveType)
	{
	case EMoveType_MirrorRow:
		return cCurrentPlayerSpecialMoveFlags.GetMirrorRowUsed();
	case EMoveType_ClearRow:
		return cCurrentPlayerSpecialMoveFlags.GetClearRowUsed();
	case EMoveType_Bomb:
		return cCurrentPlayerSpecialMoveFlags.GetBombUsed();
	default:
		return false;
	}
}

void CGame::ExecuteMove(int iInputColumn, EMoveType eSelectedMoveType)
{
	m_cBoard.FillColumn(iInputColumn, m_eCurrentPlayer, eSelectedMoveType);

	UpdateCurrentPlayerSpecialMoveFlags(eSelectedMoveType);

	m_ePreviousInputType = EInputType_ValidMove;
}

void CGame::WaitForReturnKey()
{
	system("pause");
}

bool CGame::HandleRequestAnotherGame()
{
	m_cMessagePrinter.PrintRequestAnotherGameMessage();

	char chInput;
	ReadCharFromStdIn(chInput);

	return !(chInput == 'n' || chInput == 'N');
}

CSpecialMoveFlags& CGame::GetSpecialMoveFlagsForPlayer(EPlayer ePlayer)
{
	if (ePlayer == EPlayer_P1)
	{
		return m_cP1SpecialMoveFlags;
	}
	else
	{
		return m_cP2SpecialMoveFlags;
	}
}

void CGame::UpdateCurrentPlayerSpecialMoveFlags(EMoveType eSelectedMoveType)
{
	CSpecialMoveFlags& cCurrentPlayerSpecialMoveFlags = GetSpecialMoveFlagsForPlayer(m_eCurrentPlayer);

	switch (eSelectedMoveType)
	{
	case EMoveType_MirrorRow:
		cCurrentPlayerSpecialMoveFlags.SetMirrorRowUsed(true);
		break;
	case EMoveType_ClearRow:
		cCurrentPlayerSpecialMoveFlags.SetClearRowUsed(true);
		break;
	case EMoveType_Bomb:
		cCurrentPlayerSpecialMoveFlags.SetBombUsed(true);
		break;
	default:
		break;
	}
}

void CGame::CheckForGameOver()
{
	EPlayer eWinningPlayer = m_cBoard.GetWinningPlayer();

	switch (eWinningPlayer)
	{
	case EPlayer_P1:
		m_eCurrentGameState = EGameState_P1Won;
		m_bGameOver = true;
		return;
	case EPlayer_P2:
		m_eCurrentGameState = EGameState_P2Won;
		m_bGameOver = true;
		return;
	case EPlayer_None:
		if (m_cBoard.IsBoardFull())
		{
			m_eCurrentGameState = EGameState_Draw;
			m_bGameOver = true;
		}
	}
}
