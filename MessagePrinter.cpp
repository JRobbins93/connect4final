#include "stdafx.h"
#include "MessagePrinter.h"
#include "SpecialMoveFlags.h"
#include <iostream>

using namespace std;

CMessagePrinter::CMessagePrinter()
{
}

CMessagePrinter::~CMessagePrinter()
{
}

void CMessagePrinter::PrintCurrentPlayerMessage(EPlayer eCurrentPlayer)
{
	if (eCurrentPlayer == EPlayer_P1)
	{
		cout << "P1 turn" << endl << endl;
	}
	else
	{
		cout << "P2 turn" << endl << endl;
	}
}

void CMessagePrinter::PrintPreviousInputFeedbackMessage(EInputType ePreviousInputType)
{
	switch (ePreviousInputType)
	{
	case EInputType_FullColumn:
		PrintFullColumnMessage();
		break;
	case EInputType_FailedSpecial:
		PrintFailedSpecialMessage();
		break;
	case EInputType_Invalid:
		PrintInvalidInputMessage();
		break;
	default:
		break;
	}
}

void CMessagePrinter::PrintFullColumnMessage()
{
	cout << "The column you chose was full, try again." << endl << endl;
}

void CMessagePrinter::PrintInvalidInputMessage()
{
	cout << "Your last input was invalid, try again." << endl << endl;
}

void CMessagePrinter::PrintFailedSpecialMessage()
{
	cout << "You have already used the special you selected, try again." << endl << endl;
}

void CMessagePrinter::PrintCurrentPlayerRemainingSpecialMovesMessage(CSpecialMoveFlags& cSpecialMoveFlags)
{
	bool bPlayerHasAnySpecialMovesRemaining = !(cSpecialMoveFlags.GetMirrorRowUsed() && cSpecialMoveFlags.GetClearRowUsed() && cSpecialMoveFlags.GetBombUsed());

	if (!bPlayerHasAnySpecialMovesRemaining) //If player has no special moves, don't clutter screen
	{
		return;
	}

	cout << "You have the following special moves remaining: (type the indicated letter after the column number to use them)" << endl;

	if (!cSpecialMoveFlags.GetMirrorRowUsed())
	{
		cout << "Mirror Row (m)\t\t";
	}

	if (!cSpecialMoveFlags.GetClearRowUsed())
	{
		cout << "Clear Row (c)\t\t";
	}

	if (!cSpecialMoveFlags.GetBombUsed())
	{
		cout << "Bomb (b)\t\t";
	}

	cout << endl << endl;
}

void CMessagePrinter::PrintInstructionMessage()
{
	cout << "Enter your move by typing a column number, followed by an optional special move key, then press enter. Press q to quit." << endl << endl;
}

void CMessagePrinter::PrintRequestAnotherGameMessage()
{
	cout << "Would you like to play again? (y/n) ";
}

void CMessagePrinter::PrintGameResultMessage(EGameState eFinalGameState)
{
	switch (eFinalGameState)
	{
	case EGameState_Draw:
		cout << "It's a draw!" << endl << endl;
		break;
	case EGameState_P1Won:
		cout << "Player 1 wins!" << endl << endl;
		break;
	case EGameState_P2Won:
		cout << "Player 2 wins!" << endl << endl;
		break;
	default:
		break;
	}
}

void CMessagePrinter::PrintQuitConfirmationMessage()
{
	ClearConsole();
	cout << "Are you sure you wish to quit? (y/n) ";
}

void CMessagePrinter::ClearConsole()
{
	system("cls");
}
