#ifndef STRUCTS_H_
#define STRUCTS_H_

struct SBoardSize
{
	int iBoardWidth;
	int iBoardHeight;
};

struct SGameConfiguration
{
	SBoardSize sBoardSize;
	int iWinningLineLength;
};

struct SCellCoordinates
{
	int iColumn;
	int iRow;
};

#endif