#ifndef MESSAGEPRINTER_H_
#define MESSAGEPRINTER_H_

#include "Enums.h"
#include "Struct.h"
#include "SpecialMoveFlags.h"

//Responsible for printing messages about the game to the user
class CMessagePrinter
{
private:
	//Clears the console
	void ClearConsole();

public:
	CMessagePrinter();
	~CMessagePrinter();

	//Prints a message stating that it is the current player's turn
	void PrintCurrentPlayerMessage(EPlayer eCurrentPlayer);

	//Prints a message informing the player why their previous input did not result in a valid move (if relevant)
	void PrintPreviousInputFeedbackMessage(EInputType ePreviousInputType);

	//Prints a message informing the player that they just attempted to add a token to a column that is full
	void PrintFullColumnMessage();

	//Prints a message informing the player that their last input didn't make sense
	void PrintInvalidInputMessage();

	//Prints a message informing the player that they just tried to use a special that they have used previously
	void PrintFailedSpecialMessage();

	//Prints a message informing the player of the special moves that have left, and the input modifiers required to use them
	void PrintCurrentPlayerRemainingSpecialMovesMessage(CSpecialMoveFlags& sSpecialMoveFlags);

	//Prints a message informing the player how to make a move
	void PrintInstructionMessage();

	//Prints a message asking the player to input whether they wish to play another game, called on game over and quit
	void PrintRequestAnotherGameMessage();

	//Prints a message informing the player's of who won the game that just finished
	void PrintGameResultMessage(EGameState eFinalGameState);

	//Prints a message asking the player if they are sure they want to quit
	void PrintQuitConfirmationMessage();
};

#endif