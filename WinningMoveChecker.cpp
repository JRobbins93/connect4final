#include "stdafx.h"
#include "WinningMoveChecker.h"

#include "Board.h"

CBoard* CWinningMoveChecker::GetBoard() const
{
	return m_pcBoard;
}

CWinningMoveChecker::CWinningMoveChecker(CBoard* pcBoard)
	: m_pcBoard(pcBoard)
{
}


CWinningMoveChecker::~CWinningMoveChecker()
{
	m_pcBoard = nullptr; //This class is not responible for destroying the board
}

EPlayer CWinningMoveChecker::CheckForWinningMove(SCellCoordinates sMoveCoordinates, EMoveType eMoveType, ECellState eCurrentPlayerCellState) const
{
	EPlayer eCurrentPlayer = eCurrentPlayerCellState == ECellState_P1 ? EPlayer_P1 : EPlayer_P2;

	if (eMoveType == EMoveType_Normal)
	{
		//Use optimised check, since any winning row formed must contain the token just inserted
		if (CheckIfLastMoveWasWinningMove(sMoveCoordinates))
		{
			return eCurrentPlayer;
		}
	}
	else
	{
		//Need to use brute force to check for winner as a winning row could have been formed that does not include the used slot
		if (CheckForWinningLineBruteForce(eCurrentPlayerCellState))
		{
			return eCurrentPlayer;
		}
		else
		{
			//Check player has not created a winning row for their opponent with their special move
			EPlayer eOtherPlayer = eCurrentPlayer == EPlayer_P1 ? EPlayer_P2 : EPlayer_P1;
			ECellState eOtherPlayerCellState = eCurrentPlayerCellState == ECellState_P1 ? ECellState_P2 : ECellState_P1;

			if (CheckForWinningLineBruteForce(eOtherPlayerCellState))
			{
				return eOtherPlayer;
			}
		}
	}

	return EPlayer_None;
}

bool CWinningMoveChecker::CheckIfLastMoveWasWinningMove(SCellCoordinates sLastMoveCellCoordinates) const
{
	ECellState eCurrentPlayerCellState = GetBoard()->GetCellState(sLastMoveCellCoordinates);

	//Look for a vertical, horizontal or diagonal line of winning length that includes the cell that was just filled
	return (CheckForWinningMoveInDirections(sLastMoveCellCoordinates, eCurrentPlayerCellState, EDirection::EDirection_Right, EDirection::EDirection_Left)
		|| CheckForWinningMoveInDirections(sLastMoveCellCoordinates, eCurrentPlayerCellState, EDirection::EDirection_Up, EDirection::EDirection_Down)
		|| CheckForWinningMoveInDirections(sLastMoveCellCoordinates, eCurrentPlayerCellState, EDirection::EDirection_UpRight, EDirection::EDirection_DownLeft)
		|| CheckForWinningMoveInDirections(sLastMoveCellCoordinates, eCurrentPlayerCellState, EDirection::EDirection_DownRight, EDirection::EDirection_UpLeft));
}

bool CWinningMoveChecker::CheckForWinningMoveInDirections(SCellCoordinates sLastMoveCellCoordinates, ECellState eCurrentPlayerCellState, EDirection eFirstDirection, EDirection eSecondDirection) const
{
	CBoard* pcBoard = GetBoard();
	int iWinningLineLength = pcBoard->GetWinningLineLength();
	int iCurrentLineLength = 1; //The line length is at least 1, since we count the token just inserted
	SCellCoordinates sCurrentCoordinates = sLastMoveCellCoordinates;
	EDirection eCurrentDirection = eFirstDirection;

	while (iCurrentLineLength < iWinningLineLength)
	{
		sCurrentCoordinates = pcBoard->GetNextCellCoordinatesInDirection(sCurrentCoordinates, eCurrentDirection);

		if (pcBoard->AreCoordinatesInBounds(sCurrentCoordinates) && pcBoard->GetCellState(sCurrentCoordinates) == eCurrentPlayerCellState)
		{
			++iCurrentLineLength;
		}
		else //Either gone over bounds of board, or line has been broken
		{
			if (eCurrentDirection == eFirstDirection)
			{
				//Prepare to search in the opposite direction, retaining the current line length
				eCurrentDirection = eSecondDirection;
				sCurrentCoordinates = sLastMoveCellCoordinates;
			}
			else
			{
				//Checked both directions, so can break
				break;
			}
		}
	}

	return iCurrentLineLength >= iWinningLineLength;
}

bool CWinningMoveChecker::CheckForWinningLineBruteForce(ECellState ePlayerCellState) const
{
	SBoardSize sBoardSize = GetBoard()->GetBoardSize();
	//Start from bottom left, and move up-right
	for (int iColumnIndex = 0; iColumnIndex < sBoardSize.iBoardWidth; ++iColumnIndex)
	{
		for (int iRowIndex = 0; iRowIndex < sBoardSize.iBoardHeight; ++iRowIndex)
		{
			SCellCoordinates sInitialCellCoordinates;
			sInitialCellCoordinates.iColumn = iColumnIndex;
			sInitialCellCoordinates.iRow = iRowIndex;

			//Only need to check in one direction since any lines to the left/down will already have been considered
			if (CheckForWinningLineBruteForceAlongLine(ePlayerCellState, sInitialCellCoordinates, EDirection_Right)
				|| CheckForWinningLineBruteForceAlongLine(ePlayerCellState, sInitialCellCoordinates, EDirection_Up)
				|| CheckForWinningLineBruteForceAlongLine(ePlayerCellState, sInitialCellCoordinates, EDirection_UpRight)
				|| CheckForWinningLineBruteForceAlongLine(ePlayerCellState, sInitialCellCoordinates, EDirection_DownRight))
				return true;
		}
	}
	return false;
}

bool CWinningMoveChecker::CheckForWinningLineBruteForceAlongLine(ECellState ePlayerCellState, SCellCoordinates sInitialCellCoordinates, EDirection eDirection) const
{
	CBoard* pcBoard = GetBoard();
	int iWinningLineLength = pcBoard->GetWinningLineLength();
	int iCurrentLineLength = 0;
	SCellCoordinates sCurrentCoordinates = sInitialCellCoordinates;

	while (iCurrentLineLength < iWinningLineLength)
	{
		if (pcBoard->AreCoordinatesInBounds(sCurrentCoordinates) && pcBoard->GetCellState(sCurrentCoordinates) == ePlayerCellState)
		{
			++iCurrentLineLength;
		}
		else
		{
			break;
		}

		sCurrentCoordinates = pcBoard->GetNextCellCoordinatesInDirection(sCurrentCoordinates, eDirection);
	}

	return iCurrentLineLength >= iWinningLineLength;
}
