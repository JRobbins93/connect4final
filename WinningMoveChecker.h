#ifndef WINNINGMOVECHECKER_H_
#define WINNINGMOVECHECKER_H_

#include "Struct.h"
#include "Enums.h"

class CBoard; //Both Board class and this reference each other, so need to forward declare one of them

//Responsible for checking if a provided move won the game of Connect 4
class CWinningMoveChecker
{
private:
	CBoard* m_pcBoard;

	//Returns a pointer to board to be checked for a winning move
	CBoard* GetBoard() const;

	//Looks at the adjoining cells of where the last token was inserted to see if a winning move was made, more efficient than brute force
	bool CheckIfLastMoveWasWinningMove(SCellCoordinates sLastMoveCellCoordinates) const;

	//Given a starting cell and two directions to search, looks for a winning line of cells with the same state as the one provided
	bool CheckForWinningMoveInDirections(SCellCoordinates sLastMoveCellCoordinates, ECellState eCurrentPlayerCellState, EDirection eFirstDirection, EDirection eSecondDirection) const;

	//Searches through the entire board, looking for a winning line of cells with the same state as the one provided
	bool CheckForWinningLineBruteForce(ECellState ePlayerCellState) const;

	//Called by CheckForWinningLineBruteForce, looks for a winning line starting at the given coordinates, in the given direction
	bool CheckForWinningLineBruteForceAlongLine(ECellState ePlayerCellState, SCellCoordinates sInitialCellCoordinates, EDirection eDirection) const;
	 
public:
	CWinningMoveChecker(CBoard* pcBoard);
	~CWinningMoveChecker();

	//Looks to see if either player won from the last move, favouring the player that made the move
	//Will update m_eWinningPlayer with a winner if there is one
	//Calls optimised checks for a standard move, brute force checks for special moves
	EPlayer CheckForWinningMove(SCellCoordinates sMoveCoordinates, EMoveType eMoveType, ECellState eCurrentPlayerCellState) const;
};

#endif