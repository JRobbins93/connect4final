#ifndef CONSOLEBOARDPRINTER_H_
#define CONSOLEBOARDPRINTER_H_

#include <iostream>
#include <Windows.h>
#include "Enums.h"
#include "Board.h"

//Responsible for printing a board to the console
//Note: Could have had the board handle its own rendering, but that increases the responsibilities of that class
//		The fact that all console printing functionality is contained within this single class makes the board class more portable
class CConsoleBoardPrinter
{
public:
	CConsoleBoardPrinter(CBoard& cBoard);
	~CConsoleBoardPrinter();

	//Prints the board associated with this printer object to the console
	void PrintBoard();

private:
	//Characters that are to be printed to represent tokens and spaces
	const char k_chEmptyCellRepresentation = ' ';
	const char k_chFilledCellRepresentation = 'O';

	//Characters that are to be printed to represent the frame of the board
	const char k_chSeparatorVertical = '|';
	const char k_chSeparatorHorizontal = '-';
	const char k_chSeparatorJoin = '+';

	//Colour codes used to print to the console in different colours
	const int k_iColourCodeWhite = 7;
	const int k_iColourCodeBlue = 9;
	const int k_iColourCodeRed = 12;
	const int k_iColourCodeYellow = 14;

	CBoard& m_cBoard;

	int m_iBoardWidth;
	int m_iBoardHeight;

	//Handle to the console window
	HANDLE m_hwndConsole;

	int m_eCurrentConsoleColourCode;

	//Returns the colour code to be used to print the contents of a cell with the given state
	int CellStateToColourCode(ECellState eCellState);

	//Returns the character used to represent the contents of a cell with the given state
	char CellStateToRepresentation(ECellState eCellState);

	//Clears the console window
	void ClearConsole();

	//Print a row that just shows the indices for the columns of the board
	void PrintColumnNumbersRow();

	//Prints the board frame and its contents
	void PrintBoardContents();

	//Prints a horizontal line to represent part of the board's frame
	void PrintHorizontalSeparatorRow();

	//Prints the representation of a row on the board, including the frame inbetween cells
	void PrintCellRow(int iRow);

	//Sets the colour the console will be printed in based on the given colour code
	void SetConsoleColour(int iConsoleColourCode);

};

#endif