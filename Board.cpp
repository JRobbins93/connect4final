#include "stdafx.h"
#include "Board.h"

CBoard::CBoard(SBoardSize sBoardSize, int iWinningLineLength)
	: m_sBoardSize(sBoardSize)
	, m_iWinningLineLength(iWinningLineLength)
	, m_eWinningPlayer(EPlayer_None)
	, m_cWinningMoveChecker(this)
{
	int iBoardArea = GetBoardArea();

	m_paeBoard = new ECellState[iBoardArea];

	ClearBoard(); //Initialise board to be empty
}

CBoard::~CBoard()
{
	delete[] m_paeBoard;
	m_paeBoard = nullptr;
}

void CBoard::ClearBoard()
{
	for (int iIndex = 0; iIndex < GetBoardArea(); ++iIndex)
	{
		m_paeBoard[iIndex] = ECellState::ECellState_Empty;
	}

	m_eWinningPlayer = EPlayer_None;
}

SBoardSize CBoard::GetBoardSize() const
{
	return m_sBoardSize;
}

int CBoard::GetBoardArea() const
{
	SBoardSize sBoardSize = GetBoardSize();
	return sBoardSize.iBoardHeight * sBoardSize.iBoardWidth;
}

int CBoard::GetWinningLineLength() const
{
	return m_iWinningLineLength;
}

ECellState CBoard::GetCellState(SCellCoordinates sCoordinates) const
{
	int iIndex = GetCellIndex(sCoordinates);

	return m_paeBoard[iIndex];
}

bool CBoard::IsBoardFull() const
{
	//If all columns are full, board must be full
	for (int iColumn = 0; iColumn < GetBoardSize().iBoardWidth; ++iColumn)
	{
		if (!IsColumnFull(iColumn))
		{
			return false;
		}
	}

	return true;
}

bool CBoard::IsColumnFull(int iColumn) const
{
	//If top cell is not empty, column must be full
	SCellCoordinates sTopCellCoordinates;
	sTopCellCoordinates.iColumn = iColumn;
	sTopCellCoordinates.iRow = GetBoardSize().iBoardHeight - 1;

	ECellState eCellState = GetCellState(sTopCellCoordinates);

	return eCellState != ECellState_Empty;
}

void CBoard::FillColumn(int iColumn, EPlayer ePlayer, EMoveType eMoveType)
{
	ECellState eCurrentPlayerCellState = GetCellStateForPlayer(ePlayer);
	SCellCoordinates sMoveCoordinates = GetHighestEmptyCellInColumn(iColumn);

	ExecuteMove(sMoveCoordinates, eCurrentPlayerCellState, eMoveType);
	m_eWinningPlayer = m_cWinningMoveChecker.CheckForWinningMove(sMoveCoordinates, eMoveType, eCurrentPlayerCellState);
}

EPlayer CBoard::GetWinningPlayer() const
{
	return m_eWinningPlayer;
}

bool CBoard::IsColumnIndexInBounds(int iColumn) const
{
	return iColumn >= 0 && iColumn < m_sBoardSize.iBoardWidth;
}

bool CBoard::IsRowIndexInBounds(int iRow) const
{
	return iRow >= 0 && iRow < m_sBoardSize.iBoardHeight;
}

bool CBoard::AreCoordinatesInBounds(SCellCoordinates sCoordinates) const
{
	return IsColumnIndexInBounds(sCoordinates.iColumn) && IsRowIndexInBounds(sCoordinates.iRow);
}

int CBoard::GetCellIndex(SCellCoordinates sCoordinates) const
{
	return sCoordinates.iRow * m_sBoardSize.iBoardWidth + sCoordinates.iColumn;
}

ECellState CBoard::GetCellStateForPlayer(EPlayer ePlayer) const
{
	if (ePlayer == EPlayer_P1)
	{
		return ECellState_P1;
	}
	else
	{
		return ECellState_P2;
	}
}

SCellCoordinates CBoard::GetHighestEmptyCellInColumn(int iColumn) const
{
	SCellCoordinates sCurrentCoordinates;

	for (int iRow = 0; iRow < GetBoardSize().iBoardHeight; ++iRow)
	{
		sCurrentCoordinates.iColumn = iColumn;
		sCurrentCoordinates.iRow = iRow;

		ECellState eCurrentCellState = GetCellState(sCurrentCoordinates);

		if (eCurrentCellState == ECellState_Empty)
		{
			break;
		}
	}

	return sCurrentCoordinates;
}

void CBoard::SetCellState(SCellCoordinates sCoordinates, ECellState eCellState)
{
	int iIndex = GetCellIndex(sCoordinates);
	m_paeBoard[iIndex] = eCellState;
}

void CBoard::ExecuteMove(SCellCoordinates sMoveCoordinates, ECellState eCurrentPlayerCellState, EMoveType eMoveType)
{
	switch (eMoveType)
	{
	case EMoveType_Normal:
		SetCellState(sMoveCoordinates, eCurrentPlayerCellState);
		break;
	case EMoveType_ClearRow:
		ClearRow(sMoveCoordinates.iRow);
		break;
	case EMoveType_MirrorRow:
		MirrorRow(sMoveCoordinates.iRow);
		break;
	case EMoveType_Bomb:
		Bomb(sMoveCoordinates);
		break;
	}
}

SCellCoordinates CBoard::GetNextCellCoordinatesInDirection(SCellCoordinates sStartingCoordinates, EDirection eDirection) const
{
	SCellCoordinates sResult = sStartingCoordinates;

	switch (eDirection)
	{
	case EDirection_Up:
	case EDirection_UpRight:
	case EDirection_UpLeft:
		++sResult.iRow;
		break;
	case EDirection_Down:
	case EDirection_DownRight:
	case EDirection_DownLeft:
		--sResult.iRow;
		break;
	default:
		break;
	}

	switch (eDirection)
	{
	case EDirection_UpRight:
	case EDirection_Right:
	case EDirection_DownRight:
		++sResult.iColumn;
		break;
	case EDirection_DownLeft:
	case EDirection_Left:
	case EDirection_UpLeft:
		--sResult.iColumn;
		break;
	default:
		break;
	}

	return sResult;
}

void CBoard::MirrorRow(int iRow)
{
	int iLeftIndex = 0;
	int iRightIndex = m_sBoardSize.iBoardWidth - 1;

	while (iLeftIndex < iRightIndex)
	{
		SCellCoordinates sLeftCellCoordinates;
		sLeftCellCoordinates.iColumn = iLeftIndex;
		sLeftCellCoordinates.iRow = iRow;
		SCellCoordinates sRightCellCoordinates;
		sRightCellCoordinates.iColumn = iRightIndex;
		sRightCellCoordinates.iRow = iRow;

		//Could have just used a single temp variable here, but this seems more readable
		ECellState eLeftCellState = GetCellState(sLeftCellCoordinates);
		ECellState eRightCellState = GetCellState(sRightCellCoordinates);
		SetCellState(sLeftCellCoordinates, eRightCellState);
		SetCellState(sRightCellCoordinates, eLeftCellState);

		++iLeftIndex;
		--iRightIndex;
		//Eventually left and right index will be equal (odd width board)
		//Or left will be greater than right (even length board)
	}

	CollapseRows(); //Be sure to drop down that any tokens that may be floating
}

void CBoard::ClearRow(int iRow)
{
	for (int iCurrentColumn = 0; iCurrentColumn < m_sBoardSize.iBoardWidth; ++iCurrentColumn)
	{
		SCellCoordinates sCurrentCellCoordinates;
		sCurrentCellCoordinates.iColumn = iCurrentColumn;
		sCurrentCellCoordinates.iRow = iRow;
		SetCellState(sCurrentCellCoordinates, ECellState_Empty);
	}

	CollapseRows();  //Be sure to drop down that any tokens that may be floating
}

void CBoard::Bomb(SCellCoordinates sCoordinates)
{
	for (int iColumnDelta = -1; iColumnDelta <= 1; ++iColumnDelta)
	{
		for (int iRowDelta = -1; iRowDelta <= 1; ++iRowDelta)
		{
			SCellCoordinates sCurrentCellCoordinates = sCoordinates;
			sCurrentCellCoordinates.iColumn += iColumnDelta;
			sCurrentCellCoordinates.iRow += iRowDelta;
			if (AreCoordinatesInBounds(sCurrentCellCoordinates))
			{
				SetCellState(sCurrentCellCoordinates, ECellState_Empty);
			}
		}
	}

	CollapseRows();
}

void CBoard::CollapseRows()
{
	for (int iCurrentRow = 0; iCurrentRow < m_sBoardSize.iBoardHeight - 1; ++iCurrentRow)
	{
		for (int iCurrentColumn = 0; iCurrentColumn < m_sBoardSize.iBoardWidth; ++iCurrentColumn)
		{
			SCellCoordinates sCurrentCellCoordinates;
			sCurrentCellCoordinates.iColumn = iCurrentColumn;
			sCurrentCellCoordinates.iRow = iCurrentRow;
			ECellState eCurrentCellState = GetCellState(sCurrentCellCoordinates);

			SCellCoordinates sAboveCellCoordinates;
			sAboveCellCoordinates.iColumn = iCurrentColumn;
			sAboveCellCoordinates.iRow = iCurrentRow + 1;
			ECellState eAboveCellState = GetCellState(sAboveCellCoordinates);

			if (eCurrentCellState == ECellState_Empty)
			{
				//Need to check right up to the top row, not just the row above
				while (AreCoordinatesInBounds(sAboveCellCoordinates) && eAboveCellState == ECellState_Empty)
				{
					sAboveCellCoordinates = GetNextCellCoordinatesInDirection(sAboveCellCoordinates, EDirection_Up);
					eAboveCellState = GetCellState(sAboveCellCoordinates);
				}

				if (AreCoordinatesInBounds(sAboveCellCoordinates))
				{
					SetCellState(sCurrentCellCoordinates, eAboveCellState);
					SetCellState(sAboveCellCoordinates, ECellState_Empty);
				}
			}
		}
	}
}
