#ifndef GAME_H_
#define GAME_H_

#include "Enums.h"
#include "Struct.h"
#include "Board.h"
#include "ConsoleBoardPrinter.h"
#include "MessagePrinter.h"
#include "SpecialMoveFlags.h"

//Represents a game of Connect 4
//Responsible for controlling the main game loop and handling user input
class CGame
{
public:
	CGame(SGameConfiguration sGameConfiguration);
	~CGame();

	//Begins the main game loop, exits when the user has decided to quit the application
	void RunGameLoop();

private:
	const int k_iCharsToEat = 1000;

	EPlayer m_eCurrentPlayer;
	EGameState m_eCurrentGameState;
	EMoveType m_eCurrentMoveType;
	EInputType m_ePreviousInputType;

	bool m_bQuitGame;
	bool m_bQuitApplication;
	bool m_bGameOver;

	CBoard m_cBoard;
	CConsoleBoardPrinter m_cConsoleBoardPrinter;
	CMessagePrinter m_cMessagePrinter;

	CSpecialMoveFlags m_cP1SpecialMoveFlags;
	CSpecialMoveFlags m_cP2SpecialMoveFlags;

	//Returns true if the user has quit the game, or there is a clear winner, or there is a draw, else returns false
	bool IsGameOver();

	//Resets the game, clears the board, and restores players special moves
	void Reset();

	//Resets the special moves remaining to both players
	void ResetSpecialMoveFlags();

	//Called before randomising the current player at the start of a game
	void SeedRandomNumberGeneration();

	//Sets the current player at random
	void RandomisePlayer();

	//Sets the current player to be the other player
	void SwitchCurrentPlayer();

	//Prints appropriate messages to a player before their turn
	void PrintMessages();
	
	//Takes player input, validates it, then if appropriate, executes the selected move
	void HandlePlayerMove();

	//Checks whether the move the player has selected is a valid one for the current game state
	bool ValidateMove(int iInputColumn, EMoveType eMoveType);

	//Reads a single character from the standard input, the provided chInput variable will contain the character
	void ReadCharFromStdIn(char& chInput);

	//Reads two characters from the standard input, the provided chInput variables will contain the characters
	void ReadCharFromStdIn(char& chInput1, char& chInput2);

	//Converts a char into an integer that can be used to index columns within the board
	int CharToInt(char chInput);

	//Converts a char into an enum value representing the type of move the player has chosen, used to select special moves
	EMoveType CharToMoveType(char chInput);

	//Checks whether the player has attempted to quit the game with their most recent input
	bool CheckForQuitGame(char chInput);

	//Asks the player if they are sure they wish to quit, and handling their reply
	bool ConfirmQuitGame();

	//Returns true if the player has already used the special move they have selected, else returns false
	bool CheckForPreviouslyUsedSpecialMove(EMoveType eSelectedMoveType);
	
	//Executes the move selected by the user, applying the move type they have selected at the chosen column
	void ExecuteMove(int iInputColumn, EMoveType eSelectedMoveType);
	
	//Halts execution until the user presses the return key
	void WaitForReturnKey();

	//Asks the player if they wish to play another game, called on game over or quit, handles their reply
	bool HandleRequestAnotherGame();

	//Returns the special moves flags for the provided player, used to check if player is attempting to use an already used special move
	CSpecialMoveFlags& GetSpecialMoveFlagsForPlayer(EPlayer ePlayer);

	//Updates the special move flags for the current player based on the move type selected
	void UpdateCurrentPlayerSpecialMoveFlags(EMoveType eSelectedMoveType);

	//Checks if the either player has won, or if there is a draw and updates member variables as appropriate
	void CheckForGameOver();
	

};

#endif