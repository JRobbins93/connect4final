#ifndef SPECIALMOVEFLAGS_H_
#define SPECIALMOVEFLAGS_H_

//Responsible for tracking which special moves each player has remaining to them
class CSpecialMoveFlags
{
private:
	bool m_bMirrorRowUsed;
	bool m_bClearRowUsed;
	bool m_bBombUsed;

public:
	CSpecialMoveFlags();
	~CSpecialMoveFlags();

	//Gets/sets the value indicating if the player has used their mirror row special move
	bool GetMirrorRowUsed() const;
	void SetMirrorRowUsed(bool value);

	//Gets/sets the value indicating if the player has used their clear row special move
	bool GetClearRowUsed() const;
	void SetClearRowUsed(bool value);

	//Gets/sets the value indicating if the player has used their bomb special move
	bool GetBombUsed() const;
	void SetBombUsed(bool value);

	//Resets the flags so that the player has all their moves available to them again
	void Reset();
};

#endif